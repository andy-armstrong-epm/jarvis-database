﻿CREATE TABLE [dbo].[LifeExpectancy] (
    [LEId]                INT            IDENTITY (1, 1) NOT NULL,
    [LEProviderId]        INT            NOT NULL,
    [InsuredId]           INT            NOT NULL,
    [LEDate]              DATETIME       NULL,
    [50PctLEMedian]       DECIMAL (8, 2) NULL,
    [50PctLEMean]         DECIMAL (8, 2) NULL,
    [85PctLEMedian]       DECIMAL (8, 2) NULL,
    [85PctLEMean]         DECIMAL (8, 2) NULL,
    [MortalityFactor]     DECIMAL (8, 2) NULL,
    [PrimaryDiagnosis]    VARCHAR (100)  NULL,
    [MortalityMultiplier] DECIMAL (8, 2) NULL,
    [IsActive]            BIT            CONSTRAINT [DF_LifeExpectancy_IsActive] DEFAULT ((0)) NOT NULL,
    [CreatedBy]           NVARCHAR (60)  NOT NULL,
    [CreateDate]          DATETIME       NOT NULL,
    [ModifiedBy]          NVARCHAR (60)  NULL,
    [ModifiedDate]        DATETIME       NULL,
    [CRTDate]             DATETIME       NULL,
    CONSTRAINT [PK_LifeExpectancy] PRIMARY KEY CLUSTERED ([LEId] ASC),
    CONSTRAINT [FK_LifeExpectancy_Insured] FOREIGN KEY ([InsuredId]) REFERENCES [dbo].[Insured] ([InsuredId]),
    CONSTRAINT [FK_LifeExpectancy_LEProvider] FOREIGN KEY ([LEProviderId]) REFERENCES [dbo].[LEProvider] ([LEProviderID])
);



