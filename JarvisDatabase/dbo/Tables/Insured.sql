﻿CREATE TABLE [dbo].[Insured] (
    [InsuredId]        INT            IDENTITY (1, 1) NOT NULL,
    [Surname]          NVARCHAR (150) NULL,
    [Forename]         NVARCHAR (150) NULL,
    [AddressLine1]     NVARCHAR (240) NULL,
    [AddressLine2]     NVARCHAR (240) NULL,
    [AddressLine3]     NVARCHAR (240) NULL,
    [AddressZIP]       NVARCHAR (30)  NULL,
    [AddressTown]      NVARCHAR (30)  NULL,
    [AddressType]      NVARCHAR (30)  NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DF_Insured_CreationDate] DEFAULT (getdate()) NOT NULL,
    [DateofBirth]      DATE           NULL,
    [Gender]           CHAR (1)       NOT NULL,
    [Smoker]           BIT            NULL,
    [HomePhoneNumber]  NVARCHAR (60)  NULL,
    [ModificationDate] DATETIME       NULL,
    [ModificationUser] NVARCHAR (50)  NULL,
    [CreatedBy]        NVARCHAR (60)  NOT NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([InsuredId] ASC)
);



