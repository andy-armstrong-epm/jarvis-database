﻿CREATE TABLE [dbo].[DBAudit] (
    [ClusteredID]    BIGINT           NOT NULL,
    [Actions]        NVARCHAR (1)     NULL,
    [AuditId]        UNIQUEIDENTIFIER NOT NULL,
    [ChangedColumns] NVARCHAR (MAX)   NULL,
    [KeyValue]       NVARCHAR (40)    NULL,
    [NewData]        XML              NULL,
    [OldData]        XML              NULL,
    [RevisionStamp]  DATETIME         NULL,
    [RowsAffected]   INT              NOT NULL,
    [TableName]      NVARCHAR (50)    NULL,
    [UserName]       NVARCHAR (50)    NULL,
    CONSTRAINT [PK_DBAudit] PRIMARY KEY CLUSTERED ([ClusteredID] ASC)
);

