﻿CREATE TABLE [dbo].[LEProvider] (
    [LEProviderID]   INT           IDENTITY (1, 1) NOT NULL,
    [LEProviderName] VARCHAR (100) NOT NULL,
    [Active]         BIT           NOT NULL,
    [CreatedDate]    DATETIME      NOT NULL,
    [CreatedBy]      NVARCHAR (50) NOT NULL,
    [Modified]       DATETIME      NULL,
    [ModifiedBy]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_LEProvider] PRIMARY KEY CLUSTERED ([LEProviderID] ASC)
);

