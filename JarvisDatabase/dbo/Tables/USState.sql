﻿CREATE TABLE [dbo].[USState] (
    [StateId]      INT            IDENTITY (1, 1) NOT NULL,
    [State]        NVARCHAR (255) NULL,
    [StateDesc]    NVARCHAR (255) NULL,
    [StateChinese] NVARCHAR (255) NULL,
    [StateArabic]  NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([StateId] ASC)
);

