﻿CREATE TABLE [dbo].[ApplicationErrors] (
    [ErrorId]             INT           IDENTITY (1, 1) NOT NULL,
    [ErrorSource]         VARCHAR (200) NOT NULL,
    [ErrorText]           VARCHAR (MAX) NOT NULL,
    [ErrorInnerException] VARCHAR (MAX) NULL,
    [ErrorDateTime]       DATETIME      NOT NULL,
    [ErrorUser]           VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([ErrorId] ASC)
);

