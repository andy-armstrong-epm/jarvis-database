﻿CREATE TABLE [dbo].[WordList] (
    [Word]    VARCHAR (20) NOT NULL,
    [OrderBy] AS           (CONVERT([int],Crypt_Gen_Random((2)))),
    CONSTRAINT [PK_WordList] PRIMARY KEY CLUSTERED ([Word] ASC)
);

