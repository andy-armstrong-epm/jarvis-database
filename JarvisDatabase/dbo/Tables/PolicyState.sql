﻿CREATE TABLE [dbo].[PolicyState] (
    [PolicyStateId]  INT           IDENTITY (1, 1) NOT NULL,
    [PolicyId]       INT           NOT NULL,
    [PolicyStatusId] INT           NOT NULL,
    [Name]           NVARCHAR (50) NOT NULL,
    [CreationDate]   DATETIME      NOT NULL,
    [CreatedBy]      VARCHAR (60)  NOT NULL,
    CONSTRAINT [PK_CaseState] PRIMARY KEY CLUSTERED ([PolicyStateId] ASC),
    CONSTRAINT [FK_CaseState_Case] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([PolicyId]),
    CONSTRAINT [FK_CaseState_CaseStatus] FOREIGN KEY ([PolicyStatusId]) REFERENCES [dbo].[PolicyStatus] ([PolicyStatusId])
);

