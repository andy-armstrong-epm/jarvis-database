﻿CREATE TABLE [dbo].[Fund] (
    [FundId]           INT            NOT NULL,
    [Name]             NVARCHAR (50)  NOT NULL,
    [FullName]         NVARCHAR (100) NULL,
    [FundIDPrefix]     VARCHAR (50)   NULL,
    [FundIDLastSuffix] INT            NULL,
    [ActiveFrom]       DATETIME       NULL,
    [ActiveTo]         DATETIME       NULL,
    [ModificationDate] DATETIME       NULL,
    [ModificationUser] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Fund] PRIMARY KEY CLUSTERED ([FundId] ASC)
);

