﻿CREATE TABLE [dbo].[ApplicationSettings] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [TypeName] VARCHAR (30)  NOT NULL,
    [Name]     VARCHAR (100) NOT NULL,
    [Value]    VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_ApplicationSettings] UNIQUE NONCLUSTERED ([Id] ASC, [TypeName] ASC, [Name] ASC, [Value] ASC)
);

