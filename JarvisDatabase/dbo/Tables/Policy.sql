﻿CREATE TABLE [dbo].[Policy] (
    [PolicyId]                     INT              IDENTITY (1, 1) NOT NULL,
    [PolicyFundId]                 VARCHAR (200)    NOT NULL,
    [ArchiveDate]                  DATETIME         NULL,
    [ArchivedDate]                 DATETIME         NULL,
    [AwaitingStateTransitionSince] DATETIME         NULL,
    [PolicyStatusId]               INT              CONSTRAINT [DF_Case_CaseStatusId] DEFAULT ((1)) NOT NULL,
    [PolicyTypeId]                 INT              NOT NULL,
    [OverallPolicyStatus]          CHAR (1)         NULL,
    [ClosedDate]                   DATETIME         NULL,
    [CreationDate]                 DATETIME         NULL,
    [CreationUser]                 NVARCHAR (50)    NULL,
    [CurrentStatus]                NVARCHAR (50)    NULL,
    [CurrentStatusLastUpdated]     DATETIME         NULL,
    [ModificationDate]             DATETIME         NULL,
    [ModificationUser]             NVARCHAR (50)    NULL,
    [OpenDate]                     DATETIME         CONSTRAINT [DF_Case_OpenDate] DEFAULT (getdate()) NULL,
    [ReceivedDate]                 DATETIME         NULL,
    [CarrierId]                    INT              NULL,
    [IssueDate]                    DATETIME         NULL,
    [OriginationType]              NVARCHAR (200)   NULL,
    [ConversionDate]               DATE             NULL,
    [Joint]                        BIT              NOT NULL,
    [DBOption]                     NVARCHAR (10)    NULL,
    [ROPrider]                     NVARCHAR (50)    NULL,
    [IssueState]                   NVARCHAR (50)    NULL,
    [ResidentState]                NVARCHAR (50)    NULL,
    [ProductName]                  NVARCHAR (200)   NULL,
    [Provider]                     NVARCHAR (50)    NULL,
    [Market]                       NVARCHAR (50)    NULL,
    [Seller]                       NVARCHAR (50)    NULL,
    [Servicer]                     NVARCHAR (50)    NULL,
    [LapseOn]                      NVARCHAR (10)    NULL,
    [EntitlementOrderDate]         DATETIME         NULL,
    [PurchaseDate]                 DATETIME         NULL,
    [MillimanIRR]                  DECIMAL (10, 6)  NULL,
    [FundingAdviceIRR]             DECIMAL (10, 6)  NULL,
    [MAPSValue]                    NUMERIC (18)     NULL,
    [PurchasePrice]                DECIMAL (18)     NULL,
    [Originationfee_YieldBonus]    DECIMAL (18)     NULL,
    [FaceAmount]                   DECIMAL (18)     NULL,
    [RDB]                          DECIMAL (18, 2)  NULL,
    [LoanRepayment]                DECIMAL (18, 2)  NULL,
    [Withdrawal]                   DECIMAL (18, 2)  NULL,
    [DateWDReceived]               DATETIME         NULL,
    [AdjFV]                        DECIMAL (18, 2)  NULL,
    [LnEFV]                        DECIMAL (18, 2)  NULL,
    [FaceAmount2]                  DECIMAL (18, 2)  NULL,
    [PctOfFace]                    DECIMAL (28, 18) NULL,
    [NAV_1]                        DECIMAL (10, 8)  NULL,
    [NAV_2]                        DECIMAL (10, 8)  NULL,
    [NAV_3]                        DECIMAL (10, 8)  NULL,
    [SalesPrice]                   DECIMAL (18, 2)  NULL,
    [Fees]                         DECIMAL (18, 2)  NULL,
    [Buyer]                        NVARCHAR (100)   NULL,
    [Intermediary]                 NVARCHAR (100)   NULL,
    [MVAsSale]                     DECIMAL (38, 10) NULL,
    [SaleDate]                     DATE             NULL,
    [EscrowFunded]                 DATE             NULL,
    [CashReceived]                 DATE             NULL,
    [EndOfYear]                    DECIMAL (38, 10) NULL,
    [PremiumsPaid]                 DECIMAL (38, 10) NULL,
    [DOD]                          DATE             NULL,
    [DODNAVMonth]                  DATE             NULL,
    [MVAtDOD]                      DECIMAL (18, 2)  NULL,
    [CashReceived1]                DATE             NULL,
    [DOD2]                         DATE             NULL,
    [PolicyNumber]                 NVARCHAR (30)    NULL,
    CONSTRAINT [PK_Case] PRIMARY KEY CLUSTERED ([PolicyId] ASC),
    CONSTRAINT [FK_Case_CaseStatus] FOREIGN KEY ([PolicyStatusId]) REFERENCES [dbo].[PolicyStatus] ([PolicyStatusId]),
    CONSTRAINT [FK_Policy_Carrier] FOREIGN KEY ([CarrierId]) REFERENCES [dbo].[Carrier] ([CarrierId]),
    CONSTRAINT [FK_Policy_OverallPolicyStatus] FOREIGN KEY ([OverallPolicyStatus]) REFERENCES [dbo].[OverallPolicyStatus] ([OverallPolicyStatusCode]),
    CONSTRAINT [FK_Policy_PolicyType] FOREIGN KEY ([PolicyTypeId]) REFERENCES [dbo].[PolicyType] ([PolicyTypeId])
);



