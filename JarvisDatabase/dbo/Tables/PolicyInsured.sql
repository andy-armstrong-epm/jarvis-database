﻿CREATE TABLE [dbo].[PolicyInsured] (
    [PolicyInsuredId] INT IDENTITY (1, 1) NOT NULL,
    [PolicyId]        INT NOT NULL,
    [InsuredId]       INT NOT NULL,
    CONSTRAINT [PK_CaseAgainstEmployee] PRIMARY KEY CLUSTERED ([PolicyInsuredId] ASC),
    CONSTRAINT [FK_CaseAgainstEmployee_CaseId] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([PolicyId]),
    CONSTRAINT [FK_CaseAgainstEmployee_ChargeSeverityId] FOREIGN KEY ([InsuredId]) REFERENCES [dbo].[Insured] ([InsuredId]),
    CONSTRAINT [UK_CaseAgainstEmployee] UNIQUE NONCLUSTERED ([PolicyId] ASC, [InsuredId] ASC),
    CONSTRAINT [UQ__CaseAgai__EC2C20BE1CFE380B] UNIQUE NONCLUSTERED ([PolicyInsuredId] ASC)
);

