﻿CREATE TABLE [dbo].[OverallPolicyStatus] (
    [OverallPolicyStatusCode] CHAR (1)     NOT NULL,
    [OverallPolicyStatusDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_OverallPolicyStatus] PRIMARY KEY CLUSTERED ([OverallPolicyStatusCode] ASC)
);

