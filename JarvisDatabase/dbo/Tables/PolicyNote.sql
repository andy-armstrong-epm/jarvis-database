﻿CREATE TABLE [dbo].[PolicyNote] (
    [PolicyNoteId] INT           IDENTITY (1, 1) NOT NULL,
    [PolicyId]     INT           NOT NULL,
    [Note]         VARCHAR (MAX) NOT NULL,
    [CreatedBy]    VARCHAR (50)  NOT NULL,
    [CreatedDate]  DATETIME      NOT NULL,
    CONSTRAINT [PK_CaseNote] PRIMARY KEY CLUSTERED ([PolicyNoteId] ASC),
    CONSTRAINT [FK_CaseNote_Case] FOREIGN KEY ([PolicyId]) REFERENCES [dbo].[Policy] ([PolicyId])
);

