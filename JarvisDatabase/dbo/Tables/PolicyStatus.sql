﻿CREATE TABLE [dbo].[PolicyStatus] (
    [PolicyStatusId]            INT           IDENTITY (1, 1) NOT NULL,
    [Name]                      NVARCHAR (50) NOT NULL,
    [DisplayOrder]              INT           NULL,
    [CreationDate]              DATETIME      NULL,
    [ModificationDate]          DATETIME      NULL,
    [ModificationUser]          NVARCHAR (50) NULL,
    [ParentPolicyStatusID]      INT           NULL,
    [PostName]                  NVARCHAR (50) NULL,
    [ReroutePolicyStatusId]     INT           NULL,
    [DataAction]                VARCHAR (30)  NULL,
    [SqlAction]                 VARCHAR (400) NULL,
    [ReParentPolicyStatusId]    INT           NULL,
    [AutoAdvancePolicyStatusId] INT           NULL,
    [IsVisible]                 BIT           DEFAULT ((0)) NOT NULL,
    [PolicyType]                INT           NULL,
    [WorkflowVersion]           NVARCHAR (11) NULL,
    [ReSchedulePolicyStatusId]  INT           NULL,
    [RescheduleInterval]        INT           NULL,
    CONSTRAINT [PK_CaseStatus] PRIMARY KEY CLUSTERED ([PolicyStatusId] ASC)
);



