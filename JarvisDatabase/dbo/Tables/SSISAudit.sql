﻿CREATE TABLE [dbo].[SSISAudit] (
    [AuditKey]                INT              IDENTITY (1, 1) NOT NULL,
    [ParentAuditKey]          INT              NOT NULL,
    [TableName]               VARCHAR (50)     NULL,
    [PkgName]                 VARCHAR (200)    NULL,
    [PkgGUID]                 UNIQUEIDENTIFIER NULL,
    [PkgVersionGUID]          UNIQUEIDENTIFIER NULL,
    [PkgVersionMajor]         VARCHAR (50)     NULL,
    [PkgVersionMinor]         NCHAR (10)       NULL,
    [ExecStartDT]             DATETIME         NULL,
    [ExecStopDT]              DATETIME         NULL,
    [ExecutionInstanceGUID]   UNIQUEIDENTIFIER NULL,
    [ExtractRowCnt]           INT              NULL,
    [InsertRowCnt]            INT              NULL,
    [UpdateRowCnt]            INT              NULL,
    [ErrorRowCnt]             INT              NULL,
    [TableInitialRowCnt]      INT              NULL,
    [TableFinalRowCnt]        INT              NULL,
    [TableMaxDateTime]        DATETIME         NULL,
    [SuccessfulProcessingInd] VARCHAR (50)     NULL
);

