﻿CREATE TABLE [dbo].[Log] (
    [Date]      DATETIME        NOT NULL,
    [Exception] NVARCHAR (4000) NULL,
    [ID]        INT             NOT NULL,
    [Level]     VARCHAR (20)    NOT NULL,
    [Logger]    VARCHAR (255)   NOT NULL,
    [Message]   VARCHAR (4000)  NOT NULL,
    [Thread]    VARCHAR (255)   NOT NULL
);

