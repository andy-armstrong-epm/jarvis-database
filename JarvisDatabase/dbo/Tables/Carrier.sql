﻿CREATE TABLE [dbo].[Carrier] (
    [CarrierId]        INT            IDENTITY (1, 1) NOT NULL,
    [CarrierName]      NVARCHAR (200) NOT NULL,
    [OriginalIssuer]   VARCHAR (200)  NULL,
    [CarrierAbbr]      VARCHAR (200)  NULL,
    [CarrierChinese]   NVARCHAR (500) NULL,
    [CarrierArabic]    NVARCHAR (500) NULL,
    [AmBest]           VARCHAR (50)   NULL,
    [SnP]              VARCHAR (50)   NULL,
    [Moodys]           VARCHAR (50)   NULL,
    [Fitch]            VARCHAR (50)   NULL,
    [AMBestArabic]     NVARCHAR (150) NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DF_Carrier_CreationDate] DEFAULT (getdate()) NOT NULL,
    [DisplayOrder]     INT            NULL,
    [ModificationDate] DATETIME       NULL,
    [ModificationUser] VARCHAR (50)   NULL,
    [ActiveFrom]       DATETIME       NULL,
    [ActiveTo]         DATETIME       NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([CarrierId] ASC)
);

