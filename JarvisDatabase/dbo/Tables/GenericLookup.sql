﻿CREATE TABLE [dbo].[GenericLookup] (
    [GenericLookupId]  INT            IDENTITY (1, 1) NOT NULL,
    [Text]             NVARCHAR (200) NOT NULL,
    [Type]             NVARCHAR (200) NOT NULL,
    [ClientId]         INT            NOT NULL,
    [Abbr]             NVARCHAR (40)  NULL,
    [ActiveFrom]       DATETIME       NULL,
    [ActiveTo]         DATETIME       NULL,
    [CaseTypeId]       INT            NULL,
    [CreationDate]     DATETIME       NULL,
    [DisplayOrder]     INT            NULL,
    [FraudInd]         BIT            NULL,
    [ModificationDate] DATETIME       NULL,
    [ModificationUser] NVARCHAR (50)  NULL,
    [ParentId]         INT            NULL,
    CONSTRAINT [PK_GenericLookup] PRIMARY KEY CLUSTERED ([GenericLookupId] ASC)
);

