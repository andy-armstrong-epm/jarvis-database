﻿CREATE TABLE [dbo].[PolicyType] (
    [PolicyTypeId]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (50) NULL,
    [Abbr]             NVARCHAR (12) NULL,
    [FundId]           INT           NULL,
    [ActiveFrom]       DATETIME      NULL,
    [ActiveTo]         DATETIME      NULL,
    [CreationDate]     DATETIME      CONSTRAINT [DF_CaseType_CreationDate] DEFAULT (getdate()) NOT NULL,
    [DisplayOrder]     INT           NULL,
    [ModificationDate] DATETIME      NULL,
    [ModificationUser] NVARCHAR (50) NULL,
    CONSTRAINT [PK_CaseType] PRIMARY KEY CLUSTERED ([PolicyTypeId] ASC),
    CONSTRAINT [FK_PolicyType_Fund] FOREIGN KEY ([FundId]) REFERENCES [dbo].[Fund] ([FundId])
);

