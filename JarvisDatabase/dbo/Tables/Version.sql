﻿CREATE TABLE [dbo].[Version] (
    [VersionId]     INT            IDENTITY (1, 1) NOT NULL,
    [VersionDesc]   VARCHAR (8000) NOT NULL,
    [VersionNumber] VARCHAR (50)   NULL,
    [ScriptFile]    VARCHAR (50)   NULL,
    [CreatedDate]   SMALLDATETIME  CONSTRAINT [DF_Version_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (50)   CONSTRAINT [DF_Version_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED ([VersionId] ASC)
);

